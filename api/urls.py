from django.conf.urls import patterns, url, include
from rest_framework import routers
from django.conf.urls.static import static
from api import settings

router = routers.DefaultRouter()

urlpatterns = patterns('',
    url(r'^', include(router.urls)),
    url(r'^api/', include('social.urls')),
)+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
