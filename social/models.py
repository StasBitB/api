import random
from django.db import models
from django.contrib.auth.models import User


def getRandomNumber():
    return random.randint(1, 10000000000000)


def user_directory_path(instance, filename):
    return 'pictures/{0}/{1}'.format(instance.user_id, filename)


def user_photo_directory_path(instance, filename):
    return 'photos/{0}'.format(filename)


class UserPhoto(models.Model):
    image = models.ImageField(upload_to=user_photo_directory_path)
    name = models.CharField(max_length=500)
    height = models.IntegerField(default='200')
    width = models.IntegerField(default='300')
    url = models.CharField(max_length=500)


class Image(models.Model):
    original_image = models.ImageField(upload_to=user_directory_path)
    name = models.CharField(max_length=500)
    height = models.IntegerField(default='200')
    width = models.IntegerField(default='300')
    url = models.CharField(max_length=500)


class Person(models.Model):
    photo = models.ForeignKey(UserPhoto)
    user = models.OneToOneField(User)
    status = models.CharField(max_length=500, default='english')
    town = models.CharField(max_length=500, default='english')
    language = models.CharField(max_length=500, default='english')
    first_name = models.CharField(max_length=500, default='name')
    last_name = models.CharField(max_length=500, default='last')


class Thumb(models.Model):
    height = models.IntegerField(default='200')
    width = models.IntegerField(default='300')


class AlbumList(models.Model):
    creator = models.OneToOneField(Person, related_name="album_creator")


class AlbumItem(models.Model):
    title = models.CharField(max_length=500)
    description = models.CharField(max_length=500)
    album_list = models.ForeignKey(AlbumList)
    icon = models.OneToOneField(Image)


class ImageItem(models.Model):
    thumb = models.OneToOneField(Thumb)
    image = models.OneToOneField(Image)
    album = models.ForeignKey(AlbumItem)


class Friendship(models.Model):
    person = models.ForeignKey(Person, related_name="friendship_creator")
    friend = models.ForeignKey(Person, related_name="friendship_friend")


class RequestFriendship(models.Model):
    person = models.ForeignKey(Person, related_name="requester")
    requested_person = models.ForeignKey(Person, related_name="requested_user")


