from django.contrib.auth.models import User
from django.core.serializers import json
from rest_framework import serializers
from social.models import Person, AlbumItem, Image, ImageItem, UserPhoto, RequestFriendship, Friendship


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'password', 'email')


class UserPhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserPhoto
        fields = ('id', 'name', 'height', 'width', 'url')


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ('id', 'name', 'height', 'width', 'url')


class PersonSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    photo = UserPhotoSerializer()

    class Meta:
        model = Person
        fields = ('id', 'first_name', 'last_name', 'user', 'status', 'town', 'language', 'photo')


class FriendshipSerializer(serializers.ModelSerializer):
    person = PersonSerializer(read_only=True)
    friend = PersonSerializer(read_only=True)

    class Meta:
        model = Friendship
        fields = ('person', 'friend')


class IncomeRequestSerializer(serializers.ModelSerializer):
    person = PersonSerializer(read_only=True)

    class Meta:
        model = RequestFriendship
        fields = ('person',)


class RequestSerializer(serializers.ModelSerializer):
    person = PersonSerializer(read_only=True)
    requested_person = PersonSerializer(read_only=True)

    class Meta:
        model = RequestFriendship
        fields = ('person', 'requested_person')


class OutcomeRequestSerializer(serializers.ModelSerializer):
    requested_person = PersonSerializer(read_only=True)

    class Meta:
        model = RequestFriendship
        fields = ('requested_person',)


class AlbumItemSerializer(serializers.ModelSerializer):
    icon = ImageSerializer()

    class Meta:
        model = AlbumItem
        fields = ('id', 'title', 'description', 'icon')


class ImageItemSerializer(serializers.ModelSerializer):
    image = ImageSerializer()

    class Meta:
        model = ImageItem
        fields = ('image',)


class ListData(object):
    def __init__(self, user_id, content):
        self.user_id = user_id
        self.content = content


class AlbumListSerializer(serializers.Serializer):
    user_id = serializers.CharField()
    content = AlbumItemSerializer(many=True)


class SearchResultUserListSerializer(serializers.Serializer):
    user_id = serializers.CharField()
    content = UserSerializer(many=True)


class SearchResultPersonListSerializer(serializers.Serializer):
    user_id = serializers.CharField()
    content = PersonSerializer(many=True)


class IncomeRequestListSerializer(serializers.Serializer):
    user_id = serializers.CharField()
    content = IncomeRequestSerializer(many=True)


class OutcomeRequestListSerializer(serializers.Serializer):
    user_id = serializers.CharField()
    content = OutcomeRequestSerializer(many=True)


class ImageListSerializer(serializers.Serializer):
    user_id = serializers.CharField()
    album_id = serializers.CharField()
    album_title = serializers.CharField()
    album_description = serializers.CharField()
    content = ImageItemSerializer(many=True)


class Object:
    def __init__(self, **entries):
        self.__dict__.update(entries)