from django.conf.urls import patterns, url
from social.views import create_user, get_person_info, login, create_album, get_all_albums, upload_image, get_album_image_list, \
    upload_user_photo, set_person_info,  find_person, outcome_request, outcome_request_list, outcome_request_remove, \
    income_request_list, income_request_accept, friends_list

urlpatterns = patterns('',
    url(r'^register/$', create_user),
    url(r'^login/$', login),
    url(r'^user_information/$', get_person_info),
    url(r'^create_album/$', create_album),
    url(r'^get_albums/$', get_all_albums),
    url(r'^upload_image/$', upload_image),
    url(r'^get_album_image_list/$', get_album_image_list),
    url(r'^upload_photo/$', upload_user_photo),
    url(r'^set_person_info/$', set_person_info),
    url(r'^get_user_by_name/$', find_person),
    url(r'^request_person/$', outcome_request),
    url(r'^request_person_list/$', outcome_request_list),
    url(r'^request_person_remove/$', outcome_request_remove),
    url(r'^income_request_list/$', income_request_list),
    url(r'^income_request_accept/$', income_request_accept),
    url(r'^get_friends_list/$', friends_list),

)
