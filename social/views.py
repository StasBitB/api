import re
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
import json
from django.db import IntegrityError
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from social.models import Person, AlbumList, AlbumItem, Image, ImageItem, Thumb, UserPhoto, RequestFriendship, \
    Friendship
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.permissions import IsAuthenticated
from social.serializers import PersonSerializer, AlbumItemSerializer, AlbumListSerializer, ListData, ImageSerializer, \
    ImageListSerializer, UserPhotoSerializer,  SearchResultPersonListSerializer, \
    OutcomeRequestListSerializer, RequestSerializer, FriendshipSerializer, UserSerializer, Object


class JsonResponse(HttpResponse):
    def log_in(status=0, error_message="", token="", user_name="", user_id=-1):
        content_type = 'application/json'
        content = {'status': status,
                        'error': error_message,
                        'token': token,
                        'userName': user_name,
                        'userId': user_id}
        return HttpResponse(json.dumps(content), status=None, content_type=content_type)


@api_view(['POST'])
@csrf_exempt
def upload_user_photo(request):
    if request.method == 'POST':
        photo = UserPhoto(image=request.FILES['image_file'])
        array = re.split("[=&]", str(request.POST['info']))
        d = dict(array[i:i + 2] for i in range(0, len(array), 2))
        photo.name = str(d.get('name'))
        photo.height = int(d.get('height'))
        photo.width = int(d.get('width'))
        photo.save()
        photo.url = photo.image.url
        photo.save()
    serializer = UserPhotoSerializer(photo)
    return JSONResponse(serializer.data)


@api_view(['POST'])
@csrf_exempt
def create_user(request):
    user = Object(**request.data.get("user"))
    person = Object(**request.data.get("person"))
    photo = Object(**request.data.get("photo"))
    attrs = vars(person)
    print(', '.join("%s: %s" % item for item in attrs.items()))
    try:
        new_user = User.objects.create_user(username=user.username,password=user.password,email=user.email)
        new_user.save()
        new_person = Person()
        new_person.id = new_user.id
        new_person.user = new_user
        new_photo = UserPhoto.objects.get(pk=photo.id)
        new_person.photo = new_photo
        new_person.first_name = person.first_name
        new_person.last_name = person.last_name
        new_person.town = person.town
        new_person.language = person.language
        new_person.status = person.status
        new_person.save()
        list = AlbumList()
        list.creator = new_person
        list.save()
    except IntegrityError:
        response = JsonResponse.log_in(status=0, error_message="User Already exist")
        return response
    token = Token.objects.create(user=new_user)
    response = JsonResponse.log_in(status=1, token=token.key, user_name=new_user.username,
                    user_id=new_person.id)
    return response

@api_view(['POST'])
@csrf_exempt
def login(request):
    username = request.POST.get('userName', None)
    password = request.POST.get('password', None)

    if username is not None and password is not None:
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                person = Person.objects.get(user=user)
                token = Token.objects.get(user=user)
                response = JsonResponse.log_in(status=1, token=token.key,
                                               user_name=user.username,
                                               user_id=person.id)
                return response
            else:
                return JsonResponse.log_in(status=0, error_message="Invalid User")
        else:
            return JsonResponse.log_in(status=0, error_message="Invalid Username/Password")
    else:
        return JsonResponse.log_in(status=0, error_message="Invalid Data")


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def get_person_info(request):
    person_info_id = int(request.GET.get('personInfoId', -1))
    person_id = int(request.GET.get('personId'))
    if(person_info_id == -1):
        person = Person.objects.get(id=person_id)
    else:
        person = Person.objects.get(id=person_info_id)
    serializer = PersonSerializer(person)
    return JSONResponse(serializer.data)


@api_view(['POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def set_person_info(request):
    person_id = int(request.POST.get('personId', 0))
    photo_id = int(request.POST.get('photoId', 0))
    first_name = str(request.POST.get('firstName', ''))
    second_name = str(request.POST.get('secondName', ''))
    language = str(request.POST.get('language', ''))
    town = str(request.POST.get('town', ''))
    status = str(request.POST.get('status', ''))

    person = Person.objects.get(id=person_id)
    if photo_id != 0:
        photo = UserPhoto.objects.get(pk=photo_id)
        person.photo = photo
    person.first_name = first_name
    person.last_name = second_name
    person.town = town
    person.language = language
    person.status = status
    person.save()
    serializer = PersonSerializer(person)
    return JSONResponse(serializer.data)



@api_view(['POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def create_album(request):
    person_id = int(request.POST.get('personId', ''))
    title = str(request.POST.get('title', ''))
    description = str(request.POST.get('description', ''))
    l = request.POST.getlist('images')
    album = AlbumItem()
    album.title = title
    album.description = description
    album.album_list = AlbumList.objects.get(creator_id=person_id)
    album.icon = Image.objects.get(pk=int(l[0]))
    album.save()
    for item in l:
        image = Image.objects.get(pk=int(item))
        image_item = ImageItem()
        image_item.image = image
        image_item.album = album
        thumb = Thumb()
        thumb.save()
        image_item.thumb = thumb
        image_item.save()
    serializer = AlbumItemSerializer(album)
    return JSONResponse(serializer.data)


@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def get_all_albums(request):
    person_info_id = int(request.GET.get('personInfoId', -1))
    person_id = int(request.GET.get('personId'))
    if (person_info_id == -1):
        albums_list = AlbumList.objects.get(creator_id=person_id)
    else:
        albums_list = AlbumList.objects.get(creator_id=person_info_id)
    albums = albums_list.albumitem_set.all()
    list = ListData(person_id, albums)
    serializer = AlbumListSerializer(list)
    return JSONResponse(serializer.data)


@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def get_album_image_list(request):
    person_id = int(request.GET.get('personId', ''))
    album_id = int(request.GET.get('albumId', ''))
    album_item = AlbumItem.objects.get(pk=album_id)
    images = album_item.imageitem_set.all()
    list = ListData(person_id, images)
    list.album_id = album_id
    list.album_title = album_item.title
    list.album_description = album_item.description
    serializer = ImageListSerializer(list)
    return JSONResponse(serializer.data)


@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def find_person(request):
    person_id = int(request.GET.get('personId', ''))
    requested_name = str(request.GET.get('requestedName', ''))
    result = list()

    temp1 = RequestFriendship.objects.filter(person_id=person_id).values_list("requested_person", flat=True)
    exl = Person.objects.filter(id__in=temp1)

    temp = Person.objects.all()
    if exl:
        temp = temp.exclude(id__in=exl)

    person_list = temp.exclude(id=person_id)

    friend_list = list()

    friend1 = Friendship.objects.filter(person_id=person_id)
    for friendship in friend1:
        friend_list.insert(0, friendship.friend)

    friend2 = Friendship.objects.filter(friend_id=person_id)
    for friendship in friend2:
        friend_list.insert(0, friendship.person)

    fin = [x for x in person_list if x not in friend_list]

    for person in fin:
        obj = re.search(requested_name, person.first_name, re.A)
        if obj:
            result.insert(0, person)
    person_list = ListData(person_id, result)
    serializer = SearchResultPersonListSerializer(person_list)
    return JSONResponse(serializer.data)


@api_view(['POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def outcome_request(request):
    person_id = int(request.POST.get('personId', ''))
    requested_person_id = int(request.POST.get('requestedPersonId', ''))

    request = RequestFriendship()
    request.person = Person.objects.get(id=person_id)
    request.requested_person = Person.objects.get(id=requested_person_id)
    request.save()

    serializer = RequestSerializer(request)
    return JSONResponse(serializer.data)


@api_view(['POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def outcome_request_remove(request):
    person_id = int(request.POST.get('personId', ''))
    requested_person_id = int(request.POST.get('requestedPersonId', ''))

    request = RequestFriendship.objects.get(person_id=person_id, requested_person_id=requested_person_id)
    request.delete()

    serializer = RequestSerializer(request)
    return JSONResponse(serializer.data)


@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def outcome_request_list(request):
    person_id = int(request.GET.get('personId', ''))

    try:
        request_list = RequestFriendship.objects.filter(person_id=person_id)
    except RequestFriendship.DoesNotExist:
        request_list = list()

    person_list = ListData(person_id, request_list)
    serializer = OutcomeRequestListSerializer(person_list)
    return JSONResponse(serializer.data)


@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def income_request_list(request):
    person_info_id = int(request.GET.get('personInfoId', -1))
    person_id = int(request.GET.get('personId', ''))

    if person_info_id == -1:
        requests_list = RequestFriendship.objects.filter(requested_person_id=person_id).values_list("person", flat=True)
    else:
        requests_list = RequestFriendship.objects.filter(requested_person_id=person_info_id).values_list("person", flat=True)

    exl = Person.objects.filter(id__in=requests_list)
    person_list = ListData(person_id, exl)
    serializer = SearchResultPersonListSerializer(person_list)
    return JSONResponse(serializer.data)


@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def friends_list(request):
    person_info_id = int(request.GET.get('personInfoId', -1))
    person_id = int(request.GET.get('personId', ''))
    friend_list = list()

    if person_info_id == -1:
        id = person_id
    else:
        id = person_info_id


    friend1 = Friendship.objects.filter(person_id=id)
    for friendship in friend1:
        friend_list.insert(0, friendship.friend)

    friend2 = Friendship.objects.filter(friend_id=id)
    for friendship in friend2:
        friend_list.insert(0, friendship.person)

    person_list = ListData(person_id, friend_list)
    serializer = SearchResultPersonListSerializer(person_list)
    return JSONResponse(serializer.data)


@api_view(['POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def income_request_accept(request):
    person_id = int(request.POST.get('personId', ''))
    requested_person_id = int(request.POST.get('requestedPersonId', ''))

    friend = Friendship()
    friend.person = Person.objects.get(id=person_id)
    friend.friend = Person.objects.get(id=requested_person_id)
    friend.save()

    request = RequestFriendship.objects.get(person_id=requested_person_id, requested_person_id=person_id)
    request.delete()

    serializer = FriendshipSerializer(friend)
    return JSONResponse(serializer.data)



@api_view(['POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def upload_image(request):
    if request.method == 'POST':
        image = Image(original_image=request.FILES['image_file'])
        array = re.split("[=&]", str(request.POST['info']))
        d = dict(array[i:i + 2] for i in range(0, len(array), 2))
        image.user_id = int(d.get('userId'))
        image.name = str(d.get('name'))
        image.height = int(d.get('height'))
        image.width = int(d.get('width'))
        image.save()
        image.url = image.original_image.url
        image.save()
    serializer = ImageSerializer(image)
    return JSONResponse(serializer.data)


def json_response(response_dict, status=200):
    response = HttpResponse(json.dumps(response_dict), content_type="application/json", status=status)
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Headers'] = 'Content-Type, Authorization'
    return response